import dominate
from dominate.tags import *

class Meal():
    """Class for meal"""
    def __init__(self, name):
        self.name = name
        self.allergens = []
        self.composition = []
        #print("{} has been cooked.".format(self.name))


    def addAlergen(self):
        self.allergens.append(input("Allergen name: "))

    def addComposition(self):
        self.composition.append(input("Composition name: "))

    def __str__(self):
        return self.name


class DailyMealList():#JidelniListek
    """Class for day meal list """
    def __init__(self, day, date):
        self.day = day
        self.date = date
        self.closed = False
        self.mealPerDay = 3
        self.offer = []
        self.purchased = []
        self.mealNamesTMP = ["Hovezi gulas s knedlikem a cibuli", "Rizek s bramborovym salatem", "Krupicova kase"]
        for i in self.mealNamesTMP:
            meal = Meal(i)
            self.offer.append(meal)
            self.purchased.append(0)#"""
        #self.changeOffer()
        #print()
        
    
    def new(self, date):
        print(date)

    def changeOffer(self):
        self.offer = []
        for i in range(self.mealPerDay):
            meal = Meal(input("Name of {}.meal: ".format(i+1)))
            self.offer.append(meal)
            self.purchased.append(0)

    def __str__(self):
        if not self.closed:
            self.offerStr = "Offer for {} {}:\n".format(self.day, self.date)
            i = 0
            for obj in self.offer:
                i += 1
                if obj != self.offer[-1]:
                    self.offerStr += "{}.{}    purchased:{}x\n".format(i, obj.name, self.purchased[i-1])
                else:
                    self.offerStr += "{}.{}    purchased:{}x".format(i, obj.name, self.purchased[i-1])
            return self.offerStr
        else:
            self.offerStr = "Today is closed!"
            return self.offerStr

    def dayHtml(self):
        self.offerHtml = table()
        with self.offerHtml:
            th(h2("Offer for {} {}:\n".format(self.day, self.date)))
            if not self.closed:
                i = 0
                for meal in self.offer:
                    i += 1
                    with tr():
                        td("{}.{}".format(i, meal.name))
                        td("purchased:{}x".format(self.purchased[i-1]))    
            else:
                with tr():
                    td("Today is closed!")
        print(self.offerHtml)
        return self.offerHtml

    def buy(self):#objednat
        print(self)
        choice = int(input("Number of meal: "))
        piece = int(input("Piece of meal: "))
        print()
        self.purchased[choice-1] += piece
        print(self)

    def eat(self):#snist
        print(self)
        choice = int(input("Number of meal: "))
        piece = int(input("Piece of meal: "))
        print()
        self.purchased[choice-1] -= piece
        print(self)


class Weekly():
    """Class for weekly meal list"""
    def __init__(self):
        self.mealLists = []
        #self.numDays = int(input("How many days: "))
        for i in ["Pondeli", "Utery"]:#range(self.numDays):
            day = DailyMealList(i,"10.4.98")#input("Day: "), input("Date: "))
            self.mealLists.append(day)
        self.date = "{} - {}".format(self.mealLists[0].date, self.mealLists[-1].date)
        self.offerStr = self.__str__()
        #self.tmp = self.strHtml()

    def addDay(self):
        i = 0
        for list in self.mealLists:
            print("List: {}[{}]".format(list, i))
            i += 1
        choice = int(input("Index of list to update: "))
    def __str__(self):
        for day in self.mealLists:
            self.offerStr = "Offer for {}:\n\n".format(self.date)
            i = 0
            for day in self.mealLists:
                self.offerStr += "{}\n\n".format(day)
            return self.offerStr

    def strHtml(self):
        self.offerHtml = dominate.document(title="Meal List - Buchticka Meal")
        with self.offerHtml.head:
            link(rel='stylesheet', href="http://cdn.muicss.com/mui-0.9.30/css/mui.min.css")#style.css')
            script(type='text/javascript', src="http://cdn.muicss.com/mui-0.9.30/js/mui.min.js")#script.js")

        with self.offerHtml.body:
            with div(cls="mui-container"):
                with div(cls="mui-panel"):
                    with div(style="text-align:center"):
                        h1("Meal lists")
                        for i in ['home', 'about', 'contact']:
                            with a(href='/%s.html' % i):
                                button(i.title(), style="margin-left:auto;margin-right:auto;margin-top:auto;margin-bottom:auto;",cls="mui-btn mui-btn--primary mui-btn--raised")
                    hr()
                    h1("Offer for {}:".format(self.date))
                    for day in self.mealLists:
                        day.dayHtml()
                    with div(cls="paticka", style="text-alig: center;"):
                        hr()
                        with p("Copyright &copy; 2017, Buchticka.eu Team", style="text-align: center; font-size: 75%; border=0%; padding=0%"):
                            pass #a("posta@buchticka.eu", href="mailto:posta@buchticka.eu", cls="blind")
            #print(self.offerHtml)
            return self.offerHtml
   

    
        


week = Weekly()
#pondeli = JidelniListek("Pondeli", "10.4.1998")
#print(pondeli)
#"""|"""


#week.offerStr
name = "mealList"
i = 1
while True:
    try:
        if i == 1:
            f1 = open("{}.html".format(name), "w")
        f1 = open("{}{}.html".format(name,i), "w")
        f1.write(str(week.strHtml()))#doc))
        #f1.write(str(week.offerHtml))#doc))
        f1.close()
        print("\nFILE NAME: {}{}.html".format(name, i))
        break
    except:
        print("Error with saving html file!")
        i += 1

name = "index"
while True:
    try:
        f2 = open("C:\\xampp\\htdocs\\meal\\{}.html".format(name), "w")
        f2.write(str(week.strHtml()))
        f2.close()
        print("\nFILE NAME: {}{}.html".format(name, i))
        break
    except:
        print("Error with saving html file! to C:\\xampp\\htdocs\\meal\\")

#input(".: SUCCES COMPLETED! :.")
print(".: SUCCES COMPLETED! :.")
